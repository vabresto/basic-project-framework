import os
import shutil
from subprocess import call
import sys

THIRD_PARTY_DIR_ROOT = 'third-party/'
TEMP_DIR = 'temp-third-party/'

GIT_DEFINITIONS = {
    'imgui': 
        {
            'URL': 'https://github.com/ocornut/imgui.git',
            'Temp': THIRD_PARTY_DIR_ROOT + TEMP_DIR + 'imgui/',
            'Dest': THIRD_PARTY_DIR_ROOT + 'imgui/',
            'License': 'LICENSE.txt',
            'Required-Files': [
                'imconfig.h', 'imgui.cpp', 'imgui.h', 
                'imgui_demo.cpp', 'imgui_draw.cpp', 'imgui_internal.h',
                'stb_rect_pack.h', 'stb_textedit.h', 'stb_truetype.h'],
            'Required-Folders': [],
            'Rename-Files': {},
            'Rename-Folders': {},
            'Remove-Folders': []
        },
    # Skip pynotify2 because it doesn't have a git repo (uses mercurial instead)
    #'py-notify2': 
    #    {
    #        'URL': 'https://bitbucket.org/takluyver/pynotify2',
    #        'Temp': THIRD_PARTY_DIR_ROOT + TEMP_DIR + 'py-notify2',
    #        'Dest': THIRD_PARTY_DIR_ROOT + 'py-notify2',
    #        'Required-Files': ['LICENSE', 'notify2.py'],
    #        'Required-Folders': []
    #    },
    # TODO: Revisit/finalize dbus
    #'dbus-python':
    #    {
    #        'URL': 'git://anongit.freedesktop.org/git/dbus/dbus-python',
    #        'Temp': THIRD_PARTY_DIR_ROOT + TEMP_DIR + 'dbus-python/',
    #        'Dest': THIRD_PARTY_DIR_ROOT + 'dbus-python/',
    #        'License': 'COPYING',
    #        'Required-Files': [],
    #        'Required-Folders': [''],
    #        'Rename-Files': {},
    #        'Rename-Folders': {},
    #        'Remove-Folders': []
    #    },
    'selenium': 
        {
            'URL': 'https://github.com/SeleniumHQ/selenium.git',
            'Temp': THIRD_PARTY_DIR_ROOT + TEMP_DIR + 'selenium/',
            'Dest': THIRD_PARTY_DIR_ROOT + 'selenium/',
            'License': 'LICENSE',
            'Required-Files': [],
            'Required-Folders': ['py/selenium'],
            'Rename-Files':
                {
                    'py/selenium/__init__.py': '__init__.py'
                },
            'Rename-Folders': 
                {
                    'py/selenium/common': 'common',
                    'py/selenium/webdriver': 'webdriver'
                },
            'Remove-Folders': ['py']
        },
    'SFML': 
        {
            'URL': 'https://github.com/SFML/SFML.git',
            'Temp': THIRD_PARTY_DIR_ROOT + TEMP_DIR + 'SFML/',
            'Dest': THIRD_PARTY_DIR_ROOT + 'SFML/',
            'License': 'license.md',
            'Required-Files': ['CMakeLists.txt'],
            'Required-Folders': ['cmake', 'extlibs', 'include/SFML', 'src/SFML'],
            'Rename-Files': {},
            'Rename-Folders': {},
            'Remove-Folders': []
        },
    'imgui-sfml':
        {
            'URL': 'https://github.com/eliasdaler/imgui-sfml.git',
            'Temp': THIRD_PARTY_DIR_ROOT + TEMP_DIR + 'imgui-sfml/',
            'Dest': THIRD_PARTY_DIR_ROOT + 'imgui-sfml/',
            'License': 'LICENSE',
            'Required-Files': ['imconfig-SFML.h', 'imgui-SFML.cpp', 'imgui-SFML.h'],
            'Required-Folders': [],
            'Rename-Files': {},
            'Rename-Folders': {},
            'Remove-Folders': []
        }
}

# Source: https://stackoverflow.com/questions/2656322/shutil-rmtree-fails-on-windows-with-access-is-denied
def on_error(func, path, exc_info):
    """
    Error handler for ``shutil.rmtree``.

    If the error is due to an access error (read only file)
    it attempts to add write permission and then retries.

    If the error is for another reason it re-raises the error.

    Usage : ``shutil.rmtree(path, onerror=on_error)``
    """
    import stat
    if not os.access(path, os.W_OK):
        # Is the error an access error ?
        os.chmod(path, stat.S_IWUSR)
        func(path)
    else:
        raise Exception("Unspecified error in on_error")

def make_folder_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def get_or_error(the_dict, attr):
    return the_dict[attr]
    
def clone_project(name, proj_def):
    try:
        proj_url = get_or_error(proj_def, 'URL')
        temp_dir = get_or_error(proj_def, 'Temp')

        make_folder_if_not_exists(temp_dir)

        call([
            'git',
            'clone',
            '--depth',
            '1',
            proj_url,
            temp_dir
        ])

        dest_dir = get_or_error(proj_def, 'Dest')
        make_folder_if_not_exists(dest_dir)

        required_folders = get_or_error(proj_def, 'Required-Folders')
        for folder in required_folders:
            temp_folder = temp_dir + folder
            dest_folder = dest_dir + folder
            print("Copying (folder)", temp_folder, "to", dest_folder)
            shutil.copytree(temp_folder, dest_folder)

        required_files = get_or_error(proj_def, 'Required-Files')
        for file in required_files:
            temp_file = temp_dir + file
            print("Copying (file)", temp_file, "to", dest_dir)
            shutil.copy2(temp_file, dest_dir)

        license_source_file = get_or_error(proj_def, 'License')
        init_license = temp_dir + license_source_file
        dest_license = dest_dir + 'LICENSE'
        print("Copying source file", init_license, "to", dest_license)
        shutil.copy2(init_license, dest_license)

        rename_folders = get_or_error(proj_def, 'Rename-Folders')
        for init, final in rename_folders.items():
            init_dir = dest_dir + init
            final_dir = dest_dir + final
            print("Renaming (folder)", init_dir, "to", final_dir)
            shutil.move(init_dir, final_dir)

        rename_files = get_or_error(proj_def, 'Rename-Files')
        for init, final in rename_files.items():
            init_file = dest_dir + init
            final_file = dest_dir + final
            print("Renaming (file)", init_file, "to", final_file)
            shutil.move(init_file, final_file)

        remove_folders = get_or_error(proj_def, 'Remove-Folders')
        for folder in remove_folders:
            del_folder = dest_dir + folder
            print("Deleting", del_folder)
            shutil.rmtree(del_folder, onerror=on_error)

    except Exception as e:
        print('Error: ', e)
        print("Skipped project:", name)

def make_folders():
    make_folder_if_not_exists(THIRD_PARTY_DIR_ROOT)
    make_folder_if_not_exists(THIRD_PARTY_DIR_ROOT + TEMP_DIR)

def get_all_sources():
    for name, value in GIT_DEFINITIONS.items():
        clone_project(name, value)

def clean_up():
    shutil.rmtree(THIRD_PARTY_DIR_ROOT + TEMP_DIR, onerror=on_error)

# Source: https://stackoverflow.com/questions/8428954/move-child-folder-contents-to-parent-folder-in-python
def move_to_root_folder(root_path, cur_path):
    for filename in os.listdir(cur_path):
        if os.path.isfile(os.path.join(cur_path, filename)):
            shutil.move(os.path.join(cur_path, filename), os.path.join(root_path, filename))
        elif os.path.isdir(os.path.join(cur_path, filename)):
            move_to_root_folder(root_path, os.path.join(cur_path, filename))
        else:
            sys.exit("Should never reach here.")
    # remove empty folders
    if cur_path != root_path:
        os.rmdir(cur_path)

def get_sqlite_sources():
    import zipfile
    import urllib.request

    SQLITE_SOURCE_URL = "https://www.sqlite.org/2018/sqlite-amalgamation-3230100.zip"
    SQLITE_TEMP_DEST = THIRD_PARTY_DIR_ROOT + TEMP_DIR + "sqlite/sources.zip"
    SQLITE_FINAL_DEST = THIRD_PARTY_DIR_ROOT + "sqlite/"

    if os.path.exists(SQLITE_FINAL_DEST):
        return

    make_folder_if_not_exists(THIRD_PARTY_DIR_ROOT + TEMP_DIR + "sqlite")
    make_folder_if_not_exists(SQLITE_FINAL_DEST)

    urllib.request.urlretrieve(SQLITE_SOURCE_URL, SQLITE_TEMP_DEST)

    zip_ref = zipfile.ZipFile(SQLITE_TEMP_DEST, 'r')
    zip_ref.extractall(SQLITE_FINAL_DEST)
    zip_ref.close()

    move_to_root_folder(SQLITE_FINAL_DEST, SQLITE_FINAL_DEST)


def extra_commands():
    # This could maybe be more efficient
    INCLUDE_SFML_TEXT = "#include <imconfig-SFML.h>"
    TARGET_SFML_FILE = "third-party/imgui/imconfig.h"

    with open(TARGET_SFML_FILE, "a") as file:
        file.write(INCLUDE_SFML_TEXT)

def main():
    make_folders()

    get_all_sources()

    get_sqlite_sources()

    extra_commands()

    #clean_up()

if __name__ == '__main__':
    main()