import os
from subprocess import run
import time

def make_folder_if_not_exists(directory):
    import os
    if not os.path.exists(directory):
        os.makedirs(directory)

def main():
    make_folder_if_not_exists('intermediate')
    make_folder_if_not_exists('intermediate/third-party/SFML/')
    #run("cmake -A x64 -T host=x64 -DBUILD_SHARED_LIBS=OFF ../../third-party/SFML/", cwd='intermediate/SFML/') # Can't compile static yet
    run("cmake -A x64 -T host=x64 ../../../third-party/SFML/", cwd='intermediate/third-party/SFML/')
    run("cmake -A x64 -T host=x64 -DBUILD_SHARED_LIBS=OFF ../cmake/", cwd='intermediate/')
    
if __name__ == '__main__':
    main()