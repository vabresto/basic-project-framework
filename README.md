# Basic Project Framework

Clone this Git repo for a CMake project with the following modules:

* Imgui
* SFML
* Imgui-SFML
* SQLite
* Python
* Selenium (Python)


## To Use

First, install Python. This was set up using Python 3.6.5

Then, fetch the third-party dependencies. This is done by calling `python update_third_party_sources.py`. This will fetch all of the required source code (this will take a while).

Finally, run `python __build__.py`. This will generate all of the required project files.

**NOTE:** SFML currently has to be compiled separately from the main project. Go to `intermediate/third-party/SFML/` and build the entire project. The main project (in `intermediate/`) should now compile.

To run, you will need to (manually) copy the SFML DLLs into your output folder.

## To Do

* Make SFML link statically
* Auto-fetch the required webdriver executables based on the platform
